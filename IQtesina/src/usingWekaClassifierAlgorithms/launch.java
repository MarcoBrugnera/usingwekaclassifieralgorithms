package usingWekaClassifierAlgorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import usingWekaClassifierAlgorithms.classifier.J48;
import usingWekaClassifierAlgorithms.classifier.JRip;
import usingWekaClassifierAlgorithms.classifier.MultiClassClassifier;
import usingWekaClassifierAlgorithms.classifier.NaiveBayes;
import usingWekaClassifierAlgorithms.classifier.RandomForest;
import usingWekaClassifierAlgorithms.classifier.RandomTree;
import static weka.classifiers.AbstractClassifier.runClassifier;

/**
 *
 * @author marcobrugnera
 */
public class launch {
    
    
    public static String askUserInserDatasetPath() throws IOException{
        String DatasetPath = null;
        System.out.print("Digitare il percorso del dataset da utilizzare: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean validAnsware = false;
        while (!validAnsware) {
            DatasetPath = br.readLine();
            File f = new File(DatasetPath);
            if(f.exists() && !f.isDirectory()) { 
                validAnsware = true;
            }
            else{
                System.out.print("\n Attenzione il percorso inserito non corrisponde a un file esistente: ");
            }
        }
        return DatasetPath;
    }
    
    public static int askUserToChooseTheAlgoritm(){
        int answare = -1;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Selezionare il classificatore da utilizzare. Digitare uno dei seguenti valori numerici per utilizzare il corrispondente classificatore: \n"
                + "\t 0: J48\n"
                + "\t 1: JRip\n"
                + "\t 2: MultiClassClassifier\n"
                + "\t 3: NaiveBayes\n"
                + "\t 4: RandomForest\n"
                + "\t 5: RandomTree\n");
        boolean validAnsware = false;
        while (!validAnsware) {
            try{
                answare = Integer.parseInt(br.readLine());
                if(answare < 0 | answare >5){
                    System.out.print("\n Il valore numerico digitato non rientra nell'intervallo valido [0,5]. Per continuare digitare un valore numerico compreso in [0,5]: ");
                }
                else{
                    validAnsware = true;
                    break;
                }
            }
            catch(Exception e){
                System.out.print("\n Attenzione la stringa digitata non corrisponde a un numero. Per continuare è necessario digitare un valore compreso tra [0,5]: ");  
            }
        }
        return answare;
    }
    
    public static void startClassify(int classifier, String dataset){
        String [] options;
        switch (classifier){
            case 0:
                options = ("-t " + dataset + " -C 0.25 -M 2").split(" ");
                runClassifier(new J48(), options);
                break;
            case 1:
                options = ("-t " + dataset + " -F 3 -N 2.0 -O 2 -S 1").split(" ");
                runClassifier(new JRip(), options);
                break;
            case 2:
                options = ("-t " + dataset + " -M 0 -R 2.0 -S 1 -num-decimal-places 4").split(" ");
                runClassifier(new MultiClassClassifier(), options);
                break;
            case 3:
                options = ("-t " + dataset + "").split(" ");
                runClassifier(new NaiveBayes(), options);
                break;
            case 4:
                options = ("-t " + dataset + " -I 10 -num-slots 4").split(" ");
                runClassifier(new RandomForest(), options);
                break;
            case 5:
                options = ("-t " + dataset + " -K 0 -M 1.0 -V 0.001 -S 1").split(" ");
                runClassifier(new RandomTree(), options);
                break;
        }   
    }
    
    public static void main(String[] argv) throws IOException {
        String dataset = askUserInserDatasetPath();
        int answare = askUserToChooseTheAlgoritm();
        startClassify(answare, dataset);
    }   
}
